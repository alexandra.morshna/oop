Author: Morshna Alexandra
Version: 2.0 of 2020/12/04 
(version 2.0 rework hierarchy using Carl Linnaeus taxonomy)

OOP Task
=====
With this code, you can see the multi-layer inheritance of three classes with different specifics, setters and getters, classes and methods.
In oop.py I make classes and subclasses, with some function print.
In test.py I tried to write tests for my classes and subclasses, but I do not think that is right. But i tried to explain everything in comments.

UML:
![alt text](<.\oop\images\oop1.png>) 

SG - setters&getters.